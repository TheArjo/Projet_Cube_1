describe('Test de connexion', () => {

    it('Accès à la page de connexion', () => {
        cy.visit('http://cube1.local/login')
    })

    it('Vérification du champs "mail"',  () => {
        cy.get('#username')
            .should('have.attr', 'type', 'text')
    })

    it('Saisie de l\'adresse mail' ,  () => {
        cy.get('#username').type('test@test.com')
    });

    it('Vérification du champs "Mot de passe"',  () => {
        cy.get('#passe').should('have.attr', 'type', 'password')
    })

    it('Saisie du mot de passe' ,  () => {
        cy.get('#passe').type('Test00')
    });

    it('Clic de validation du formulaire',  () => {
        cy.get('input[type=submit]').click()
    });
})
beforeEach(() => {
    // before each test, we can automatically preserve the
    // 'session_id' and 'remember_token' cookies. this means they
    // will not be cleared before the NEXT test starts.
    //
    // the name of your cookies will likely be different
    // this is an example
    Cypress.Cookies.preserveOnce('PHPSESSID', 'remember_token')
});

describe('Test d\'ajout d\'une ressource', () => {

    it('Accès à la page d\'ajout de ressource', () => {
        cy.visit('http://cube1.local/ressource/add')
    });

    it('Saisie du nom de la ressource' ,  () => {
        cy.get('#ressource_libelle_ressource').type('Exemple')
    });

    it('Sélection de la catégorie de la ressource' ,  () => {
        cy.get('#ressource_categorie')
            .select(3)
    });

    it('Sélection du type de relation' ,  () => {
        cy.get('#ressource_type_relation')
            .select(0)
    });

    describe('Test des types de ressources' ,  () => {

        it('Sélection type "Article"' ,  () => {
            cy.get('#ressource_type_ressource')
                .select(1)
        });

        it('Présence du champs "Lien" ' ,  () => {
            cy.get('#ressource_lien')
                .should('have.attr', 'type', 'text')
        });

        it('Sélection type "Carte défi"' ,  () => {
            cy.get('#ressource_type_ressource')
                .select(2)
        });

        it('Présence du bouton de téléchargement de fichier" ' ,  () => {
            cy.get('#ressource_fileImg')
                .should('have.attr', 'type', 'file')
        });

        it('Sélection type "Cours au format PDF" ' ,  () => {
            cy.get('#ressource_type_ressource')
                .select(3)
        });

        it('Présence du bouton de téléchargement de fichier" ' ,  () => {
            cy.get('#ressource_filePDF')
                .should('have.attr', 'type', 'file')
        });

        it('Sélection type "Exercice/Atelier" ' ,  () => {
            cy.get('#ressource_type_ressource')
                .select(4)
        });

        it('Présence du bouton de téléchargement de fichier" ' ,  () => {
            cy.get('#ressource_fileFichier')
                .should('have.attr', 'type', 'file')
        });

        it('Sélection type "Fiche de lecture" ' ,  () => {
            cy.get('#ressource_type_ressource')
                .select(5)
        });

        it('Présence du bouton de téléchargement de fichier" ' ,  () => {
            cy.get('#ressource_fileFichier')
                .should('have.attr', 'type', 'file')
        });

        it('Sélection type "Jeu en ligne ' ,  () => {
            cy.get('#ressource_type_ressource')
                .select(6)
        });

        it('Présence du champs "Lien" ' ,  () => {
            cy.get('#ressource_lien')
                .should('have.attr', 'type', 'text')
        });

        it('Sélection type "Activité / Jeu à réaliser" ' ,  () => {
            cy.get('#ressource_type_ressource')
                .select(0)
        });

        it('Présence du bouton de téléchargement de fichier" ' ,  () => {
            cy.get('#ressource_fileFichier')
                .should('have.attr', 'type', 'file')
        });

        it('Sélection type "Vidéo ' ,  () => {
            cy.get('#ressource_type_ressource')
                .select(7)
        });

        it('Présence du champs "Lien" ' ,  () => {
            cy.get('#ressource_lienVideo')
                .type("https://www.youtube.com/watch?time_continue=19&v=AJkXIFVG1rk")
                .should('have.attr', 'type', 'text')

        });
        it('Validation d\'ajout de la ressource" ' ,  () => {
            cy.get('#ressource_ajouter').click()
        });

    });


})

