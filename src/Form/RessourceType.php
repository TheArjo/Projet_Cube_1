<?php

namespace App\Form;

use App\Entity\Ressource;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RessourceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('libelle_ressource')
            ->add('categorie')
            ->add('type_relation')
            ->add('type_ressource')
            ->add('fileFichier', FileType::class,[
                'label' => 'Ajouter une ressource',
                'mapped' => false,
                'required' => false,
            ])
            ->add('filePDF', FileType::class,[
                'label' => 'Ajouter une ressource',
                'mapped' => false,
                'required' => false,
            ])
            ->add('fileImg', FileType::class,[
                'label' => 'Ajouter une ressource',
                'mapped' => false,
                'required' => false,
            ])
            ->add('lien', TextType::class, array(
                'mapped' => false,
                'required' => false,
            ))
            ->add('lienVideo', TextType::class, array(
                'mapped' => false,
                'required' => false,
            ))
            ->add('ajouter', SubmitType::class)
            ->add('annuler', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ressource::class,
        ]);
    }
}
