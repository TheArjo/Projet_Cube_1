<?php

namespace App\Tests;

use App\Entity\Utilisateur;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testCreateUserOK(): void
    {
        $user = new Utilisateur();
        $mail = 'test@test.fr';
        $user->setEmail($mail);
        $name = 'TEST';
        $user->setNom($name);
        $firstName = 'Test';
        $user->setPrenom($firstName);
        $pseudo = 'UserTest';
        $user->setPseudo($pseudo);

        $userMail = $user->getEmail();
        $userName = $user->getNom();
        $userFirstName = $user->getPrenom();
        $userPseudo = $user->getPseudo();

        $test = ($userMail == $mail && $userName == $name && $userFirstName == $firstName && $userPseudo == $pseudo);

        $this->assertTrue($test);
    }

    public function testCreateUserKO(): void
    {
        $user = new Utilisateur();
        $mail = 'test@test.fr';
        $name = 'TEST';
        $user->setNom($name);
        $firstName = 'Test';
        $user->setPrenom($firstName);
        $pseudo = 'UserTest';
        $user->setPseudo($pseudo);

        $userMail = $user->getEmail();
        $userName = $user->getNom();
        $userFirstName = $user->getPrenom();
        $userPseudo = $user->getPseudo();

        $test = ($userMail == $mail && $userName == $name && $userFirstName == $firstName && $userPseudo == $pseudo);

        $this->assertFalse($test);
    }
}